using Godot;
using System.Linq;
using System.Collections.Generic;

[Tool] public class ParticlesMultiTexture : Node2D
{
    private bool _Emitting = false;

    [Export] public bool Emitting
    {
        get { return _Emitting; }
        set {
            _Emitting = value;
            foreach (var child in GetChildren())
                (child as Particles2D).Emitting = value;
        }
    }

    private List<Texture> _Textures;

    [Export((PropertyHint) 24, "17/17:Texture")] public Godot.Collections.Array<Texture> Textures;

    [Export] public PackedScene Particles;

    
    private bool HasChanged()
    {
        if (_Textures == null) return true;
        if (_Textures.Count() != Textures.Count()) return true;

        for (int i = 0; i < Textures.Count(); i++)
        if (_Textures[i] != Textures[i]) return true;
        
        return false;
    }

    private void BuildChildren()
    {
        foreach (var t in Textures)
        {
            var child = Particles.Instance() as Particles2D;
            if (child == null) return;

            child.Amount /= Textures.Count();
            child.Texture = t;
            AddChild(child);
        }
    }

    private void Refresh()
    {
        if (!HasChanged()) return;
        foreach (var t in Textures) t.Dispose();
        _Textures = Textures.ToList();
        BuildChildren();
    }
    
    public override void _Ready()
    {
        Emitting = _Emitting;
        Refresh();
    }

    public override string _GetConfigurationWarning()
    {
        if (Particles == null || !(Particles.Instance() is Particles2D))
        {
            return "The property Particles has to be a Particles2D scene.";
        }

        if (Textures.Count() == 0)
        {
            return "There should be at least one texture.";
        }

        return "";
    }

}
