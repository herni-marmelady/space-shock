using Godot;
using System;

public class Player : Rocket
{

    private bool IsPressed(string key)
    {
        return Input.IsActionPressed(key);
    }

    public override void _PhysicsProcess(float delta)
    {
        switch( (IsPressed("turn_right"), IsPressed("turn_left")) )
        {
            case (true, false):
                DoRotate = 1;
                break;

            case (false, true):
                DoRotate = -1;
                break;

            case (true, true):
                DoRotate = AngularVelocity > 0 ? -1 : AngularVelocity < 0 ? 1 : 0;
                break;
        }

        switch( (IsPressed("thrust_forward"), IsPressed("thrust_back")) )
        {
            case (true, false):
                DoThrust = 1;
                break;

            case (false, true):
                DoThrust = -1;
                break;
        }

        switch( (IsPressed("thrust_right"), IsPressed("thrust_left")) )
        {
            case (true, false):
                DoStrafe = 1;
                break;

            case (false, true):
                DoStrafe = -1;
                break;
        }

        
        DoPhysics(delta);
    }
}
