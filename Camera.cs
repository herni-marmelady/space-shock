using Godot;
using System;

public class Camera : Camera2D
{
    [Export] public float ZoomSpeed = .1f;

    public override void _Process(float delta)
    {
        if (Input.IsActionPressed("zoom_out"))
            Zoom *= 1 + ZoomSpeed;

        if (Input.IsActionPressed("zoom_in"))
            Zoom *= 1 - ZoomSpeed;
    }
}
