using Godot;
using System;

public class Rocket : RigidBody2D
{

    [Export] public float MaxTorque = 50;
    [Export] public float MaxForce = 3;
    [Export] public float MaxStrafe = 3;
    [Export] public float Health = 100;
    [Export] public float Toughness = 100;
    [Export] public float ShearToughness = 20;

    /// <summary>Forwards (1), none (0), backwards (-1)</summary>
    public int DoThrust = 0;

    /// <summary>CW (1), none (0), CCW (-1)</summary>
    public int DoRotate = 0;

    /// <summary>Right (1), none (0), left (-1)</summary>
    public int DoStrafe = 0;


    public void DoPhysics(float delta)
    {
        ApplyTorqueImpulse(DoRotate * MaxTorque);
        ApplyCentralImpulse(DoThrust * MaxForce * ForwardVec);
        ApplyCentralImpulse(DoStrafe * MaxStrafe * RightVec);

        DoThrust = DoRotate = DoStrafe = 0;
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {

        var col = state.GetContactCount();

        for (int i = 0; i < col; i++)
        {
            var body = state.GetContactColliderObject(i) as RigidBody2D;
            if (body == null) continue;

            var collisionPoint = state.GetContactColliderPosition(i);
            var R = collisionPoint - Position;

            var body_vel = state.GetContactColliderVelocityAtPosition(i);
            var self_vel = LinearVelocity + AngularVelocity * R.Ortho();
            var rel_vel = body_vel - self_vel;

            var normal = state.GetContactLocalNormal(i);

            var normalStress = Math.Abs(normal.Dot(rel_vel));
            var shearStress = Math.Abs(normal.Cross(rel_vel));

            Health -= (normalStress / Toughness).Sq();
            Health -= (shearStress / ShearToughness / 100).Sq();
        }
        
    }

    public Vector2 ForwardVec
    {
        get { return new Vector2(Mathf.Sin(Rotation), -Mathf.Cos(Rotation)); }
        set { Rotation = Mathf.Atan2(value[1], value[2]); }
    }

    public Vector2 RightVec
    {
        get { return new Vector2(Mathf.Cos(Rotation), Mathf.Sin(Rotation)); }
    }
}
