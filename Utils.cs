using Godot;
using System;

public static class Utils
{
    public static T GetRef<T>(this Node node, NodePath path)
    where T: Godot.Node
    {
        return (T)node.GetNode(path) ?? throw new Exception("Node not found: " + path);
    }

    public static Vector2 Ortho(this Vector2 v)
    {
        return new Vector2(-v[1], v[0]);
    }

    public static float Sq(this float a)
    {
        return a*a;
    }
}