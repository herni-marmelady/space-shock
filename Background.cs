using Godot;
using System;

public class Background : Sprite
{

    [Export] public float ParallaxSpeed = .1f;
    private Camera2D Camera;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Camera = GetParent<Camera2D>();
    }

    public override void _Process(float delta)
    {
        Rotation = -Camera.GlobalRotation;
        Scale = Camera.Zoom;

        var offset = (-Camera.GlobalPosition * ParallaxSpeed);
        var size = this.Texture.GetSize();
        var width  = size[0] * Scale[0];
        var height = size[1] * Scale[1];

        Position = new Vector2(
            offset[0] % width,
            offset[1] % height
        ).Rotated(Rotation);
    }
}
