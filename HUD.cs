using Godot;
using System;



public class HUD : Node2D
{

    private Player Player;
    private TextureProgress Healthbar;

    public override void _Ready()
    {
        Player = this.GetRef<Player>("/root/Game/Player");
        Healthbar = this.GetRef<TextureProgress>("Healthbar");
    }

    public override void _Process(float delta)
    {
        Healthbar.Value = Player.Health;
    }
}
